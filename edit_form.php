<!DOCTYPE html>
<?php
$id=$_GET['id'];

include_once 'db_config.php';
$sql = "SELECT * FROM `form` WHERE id='$id'";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
	
     // output data of each row

     while($row = $result->fetch_assoc())
		{
			
			$form_info = json_decode($row["form_info"], True);
			$tables = json_decode($row["tables"], True);			

        }
						}


?>
<html>
<head>
  <title>afco</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/font-awesome.min.css">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <style>
  td{border:1px solid #888;text-align:center;font-size: 85%;}
  td input{width:80%;text-align:center;font-size: 85%;} 
  label{font-size:75%;margin-bottom:0px;}
  hr{margin-top:5px!important;margin-bottom:5px!important;}
  div select{font-size: 75%;}
  .show-header{display:none;}
@media print {
.show-header{display:block;}
}
  </style>
</head>
<body style="background-color:#eee;">
<form action="../afco/edit.php" method="post" style="padding:15px;">
<div id="form" class="row" style="border:2px solid #888;margin-right: 10px;margin-left: 10px;padding: 5px;background-color:#fff;">

<div class="col-xs-4">
<img src="../afco/images/acfo_logo.png" style="width:100%;">
</div>

<div class="col-xs-4" style="text-align:center;">
<br>
<label>AL Hanoof Factory for Medical & lab Supplies</label><br>
<label>Weighing of injected solutions test report</label><br>
</div>

<div class="col-xs-4" >
<div class="row" style="border-bottom:1px solid #888;font-size: 60%;">
<div class="col-xs-4">Doc No:</div>
<div class="col-xs-8"><input value="<?php echo $form_info['doc_no']; ?>" name="doc_no" style="width:100%;" type="text"></div>
</div>

<div class="row" style="border-bottom:1px solid #888;font-size: 60%;">
<div class="col-xs-4">Issue Date:</div>
<div class="col-xs-8"><input value="<?php echo $form_info['issue_date']; ?>" name="issue_date" style="width:100%;" type="date"></div>
</div>

<div class="row" style="border-bottom:1px solid #888;font-size: 60%;">
<div class="col-xs-4">Effective Date:</div>
<div class="col-xs-8"><input value="<?php echo $form_info['effictive_date']; ?>" name="effictive_date" style="width:100%;" type="date"></div>
</div>

<div class="row" style="border-bottom:1px solid #888;font-size: 60%;">
<div class="col-xs-4">Rev No:</div>
<div class="col-xs-8"> <input value="<?php echo $form_info['rev_no']; ?>" name="rev_no" style="width:100%;" type="text"></div>
</div>

<div class="row" style="border-bottom:1px solid #888;font-size: 60%;">
<div class="col-xs-4">Next Rev Date:</div>
<div class="col-xs-8"> <input value="<?php echo $form_info['next_rev_date']; ?>" name="next_rev_date"  style="width:100%;" type="date"></div>
</div>


<div class="row" style="font-size: 60%;">
<div class="col-xs-4">Supersede Date:</div>
<div class="col-xs-8"> <input value="<?php echo $form_info['supersede_date']; ?>" name="supersede_date" style="width:100%;" type="text"></div>
</div>

</div>
<hr style="border-bottom:2px solid #888;width: 100%;">

<div class="row">
<div class="col-xs-3"><label  style="width:100%;">Test Starting Date/Time:</label></div>
<div class="col-xs-6"><label  style="width:100%;"><input value="<?php echo $form_info['test_start_date']; ?>" name="test_start_date" style="width:90%;" type="datetime-local"></label></div><br>
</div>

<div class="row">
<div class="col-xs-3"><label  style="width:100%;">Product Name:</label></div>
<div class="col-xs-6"><label  style="width:100%;"><input value="<?php echo $form_info['product_name']; ?>" name="product_name" style="width:90%;" type="text"></label></div>
<div class="col-xs-1" style="text-align: right;"><label  style="width:100%;">Batch No:</label></div>
<div class="col-xs-2"><label  style="width:100%;"><input value="<?php echo $form_info['batch_no']; ?>" name="batch_no" style="width:90%;" type="text"></label></div><br>
</div>

<div class="row">
<div class="col-xs-3"><label  style="width:100%;">Batch size:</label></div>
<div class="col-xs-6"><label  style="width:100%;"><input value="<?php echo $form_info['batch_size']; ?>" name="batch_size" style="width:90%;" type="text"></label></div>
<div class="col-xs-1" style="text-align: right;"><label  style="width:100%;">Cat No:</label></div>
<div class="col-xs-2"><label  style="width:100%;"><input value="<?php echo $form_info['cat_no']; ?>" name="cat_no" style="width:90%;" type="text"></label></div><br>
</div>

<div class="row">
<div class="col-xs-3"><label  style="width:100%;">Expire Date:</label></div>
<div class="col-xs-6"><label  style="width:100%;"><input value="<?php echo $form_info['expire_date']; ?>" name="expire_date" style="width:90%;" type="month"></label></div><br>
</div>

<div class="row">
<div class="col-xs-3"><label  style="width:100%;">Injected solution acceptance tolerance:</label></div>
<div class="col-xs-6"><label  style="width:100%;"><input value="<?php echo $form_info['injected_solution']; ?>" id="injected_solution" name="injected_solution" style="width:20%;" type="text">&nbsp;<label style="width:25%">gm. to</label>
<input value="<?php echo $form_info['injected_solution2']; ?>" id="injected_solution2" name="injected_solution2" style="width:20%;" type="text">&nbsp;<label style="width:25%"> gm.</label>
</label></div><br>
</div>


<div class="row">
<div class="col-xs-3"><label  style="width:100%;">Used balancename:</label></div>
<div class="col-xs-6">
	<select id="id_no1-1" onchange="changeID($(this).children(':selected').attr('id'))" value="<?php echo $form_info['id_no1-1']; ?>" name="id_no1-1" style="width: 90%;margin-top: 1px;">
     <option id="1" value="1" <?php if($form_info['id_no1-1'] == 1){echo "selected";}; ?>>ADAM CBK8 H Balance, Serial No.: AE682961</option> 
     <option id="2" value="2" <?php if($form_info['id_no1-1'] == 2){echo "selected";}; ?>>ADAM CBK8 H Balance, Serial No.: AE6821711</option> 
     <option id="3" value="3" <?php if($form_info['id_no1-1'] == 3){echo "selected";}; ?>>ADAM CBK8 H Balance, Serial No.: AE6829659</option> 
     <option id="4" value="4" <?php if($form_info['id_no1-1'] == 4){echo "selected";}; ?>>ADAM CBK4 Balance, Serial No.: AE5961859</option> 
     <option id="5" value="5" <?php if($form_info['id_no1-1'] == 5){echo "selected";}; ?>>ADAM CBK4 Balance, Serial No.: AE5961868</option> 
     <option id="6" value="6" <?php if($form_info['id_no1-1'] == 6){echo "selected";}; ?>>ADAM CBK4 Balance</option> 
     <option id="7" value="7" <?php if($form_info['id_no1-1'] == 7){echo "selected";}; ?>>EQ-120 Balance, Serial No.: 19304712</option> 
     <option id="8" value="8" <?php if($form_info['id_no1-1'] == 8){echo "selected";}; ?>>Denever TP-303 Balance, , Serial No.: 26208774</option> 
     <option id="9" value="9" <?php if($form_info['id_no1-1'] == 9){echo "selected";}; ?>>AE ADAM Balance, Serial No.: AE30L1402.</option> 
	</select><!--<label  style="width: 90%;border: 1px solid #aaa;"> Adam balance </label>-->
</div>
<div class="col-xs-1" style="text-align: right;"><label  style="width:100%;">ID No:</label></div>
<div class="col-xs-2">
    <select id="id_no1" name="id_no1" value="<?php echo $form_info['id_no1']; ?>" onchange="changeBalancename($(this).children(':selected').attr('id'))" style="width:90%;margin-top: 1px;" >
     <option id="1" value="1" <?php if($form_info['id_no1'] == 1){echo "selected";}; ?>>BAL-001</option> 
     <option id="2" value="2" <?php if($form_info['id_no1'] == 2){echo "selected";}; ?>>BAL-002</option> 
     <option id="3" value="3" <?php if($form_info['id_no1'] == 3){echo "selected";}; ?>>BAL-003</option> 
     <option id="4" value="4" <?php if($form_info['id_no1'] == 4){echo "selected";}; ?>>BAL-004</option> 
     <option id="5" value="5" <?php if($form_info['id_no1'] == 5){echo "selected";}; ?>>BAL-005</option> 
     <option id="6" value="6" <?php if($form_info['id_no1'] == 6){echo "selected";}; ?>>BAL-006</option> 
     <option id="7" value="7" <?php if($form_info['id_no1'] == 7){echo "selected";}; ?>>BAL-007</option> 
     <option id="8" value="8" <?php if($form_info['id_no1'] == 8){echo "selected";}; ?>>BAL-008</option> 
     <option id="9" value="9" <?php if($form_info['id_no1'] == 9){echo "selected";}; ?>>BAL-009</option> 
	</select>
</div><br>
</div>
		<script>
		function changeID(xx)
         { 
            $("#id_no1").val(xx);
		 }     
 
         function changeBalancename(yy)
         {
             $("#id_no1-1").val(yy);
         }
 
         function changeID2(xx)
         { 
            $("#id_no2").val(xx);
		 }

         function changeMachine(yy)
         {
             $("#id_no2-1").val(yy);
         }
		</script>
<div class="row">
<div class="col-xs-3"><label  style="width:100%;">Used filling machine name:</label></div>
<div class="col-xs-6">
	<select id="id_no2-1" onchange="changeID2($(this).children(':selected').attr('id'))" value="<?php echo $form_info['id_no2-1']; ?>" name="id_no2-1" style="width: 90%;margin-top: 1px;">
     <option id="1" value="1" <?php if($form_info['id_no2-1'] == 1){echo "selected";}; ?>>Precision spraying Machine</option> 
     <option id="2" value="2" <?php if($form_info['id_no2-1'] == 2){echo "selected";}; ?>>AXT-KT10 Automatic Adjustable Quantitative Spray & Filling System.</option> 
	</select>
</div>
<div class="col-xs-1" style="text-align: right;"><label  style="width:100%;">ID No:</label></div>
<div class="col-xs-2">
    <select id="id_no2" onchange="changeMachine($(this).children(':selected').attr('id'))" name="id_no2" value="<?php echo $form_info['id_no2']; ?>" style="width:90%;margin-top: 1px;" >
     <option id="1" value="1" <?php if($form_info['id_no2'] == 1){echo "selected";}; ?>>PRO2-017</option> 
     <option id="2" value="2" <?php if($form_info['id_no2'] == 2){echo "selected";}; ?>>PRO2-004</option> 
	</select></div><br>
</div>
<hr  style="border-bottom:2px solid #888;width: 100%;">

<div class="row" id="forms">

<?php
$i= 1;
 foreach($tables as $val) { 
?>

<?php 

if ($i == 5 || $i== 9 || $i== 13 || $i== 17 || $i== 21 || $i== 25 || $i== 29){
?>
</head>
  <style>
  td{border:1px solid #888;text-align:center;font-size: 98%;}
  td input{width:80%;text-align:center;font-size: 98%;} 
  label{font-size:75%;margin-bottom:0px;}
  hr{margin-top:5px!important;margin-bottom:5px!important;}
  div select{font-size: 75%;}
  </style>
</head>

<?php 

if ($i>=9){
?>
<div class="col-xs-12 show-header" style="height:181px;"></div>
<?php }?> 
<div class="col-xs-12 show-header">
<hr style="border-bottom:2px solid #888;width: 100%;">
<div class="col-xs-4">
<img src="../afco/images/acfo_logo.png" style="width:100%;">
</div>

<div class="col-xs-4" style="text-align:center;">
<br>
<label>AL Hanoof Factory for Medical & lab Supplies</label><br>
<label>Weighing of injected solutions test report</label><br>
</div>

<div class="col-xs-4" >
<div class="row" style="border-bottom:1px solid #888;font-size: 60%;">
<div class="col-xs-4">Doc No:</div>
<div class="col-xs-8"><input value="<?php echo $form_info['doc_no']; ?>" name="doc_no" style="width:100%;" type="text"></div>
</div>

<div class="row" style="border-bottom:1px solid #888;font-size: 60%;">
<div class="col-xs-4">Issue Date:</div>
<div class="col-xs-8"><input value="<?php echo $form_info['issue_date']; ?>" name="issue_date" style="width:100%;" type="date"></div>
</div>

<div class="row" style="border-bottom:1px solid #888;font-size: 60%;">
<div class="col-xs-4">Effective Date:</div>
<div class="col-xs-8"><input value="<?php echo $form_info['effictive_date']; ?>" name="effictive_date" style="width:100%;" type="date"></div>
</div>

<div class="row" style="border-bottom:1px solid #888;font-size: 60%;">
<div class="col-xs-4">Rev No:</div>
<div class="col-xs-8"> <input value="<?php echo $form_info['rev_no']; ?>" name="rev_no" style="width:100%;" type="text"></div>
</div>

<div class="row" style="border-bottom:1px solid #888;font-size: 60%;">
<div class="col-xs-4">Next Rev Date:</div>
<div class="col-xs-8"> <input value="<?php echo $form_info['next_rev_date']; ?>" name="next_rev_date"  style="width:100%;" type="date"></div>
</div>


<div class="row" style="font-size: 60%;">
<div class="col-xs-4">Supersede Date:</div>
<div class="col-xs-8"> <input value="<?php echo $form_info['supersede_date']; ?>" name="supersede_date" style="width:100%;" type="text"></div>
</div>

</div>
<hr style="border-bottom:2px solid #888;width: 100%;">
</div>
<?php } ?>






<div class="col-xs-6 dawood" >
<table style="width:100%;margin-bottom: 20px;">
<tr><td rowspan="3">Tube NO</td><td colspan="4" ><?php if($i == 1){echo "Test result at start up";}else{echo "Test Result At:";} ?></td></tr>
<tr><td>Date & Time:</td><td colspan="3" ><input value="<?php echo $val['test_date'.$i.'']; ?>" name="test_date<?php echo $i ; ?>" style="width:100%;" type="datetime-local"></td></tr>

<tr>

<td>ETW<span style="font-size: 75%;vertical-align: top;">3</span></td>
<td>FTW<span  style="font-size: 75%;vertical-align: top;">2</span></td>
<td>SOL-W<span  style="font-size: 75%;vertical-align: top;">1</span></td>
<td >&nbsp;&nbsp;result&nbsp;&nbsp;</td>
</tr>

<tr>
<td>N1</td>
<td><input id="n1_etw_<?php echo $i ; ?>"  value="<?php echo $val['n1_etw_'.$i.'']; ?>" name="n1_etw_<?php echo $i ; ?>" onchange="minus('n1_etw_<?php echo $i ; ?>','n1_ftw_<?php echo $i ; ?>','n1_sol_<?php echo $i ; ?>');" type="text"></td>
<td><input id="n1_ftw_<?php echo $i ; ?>"  value="<?php echo $val['n1_ftw_'.$i.'']; ?>" name="n1_ftw_<?php echo $i ; ?>" onchange="minus('n1_etw_<?php echo $i ; ?>','n1_ftw_<?php echo $i ; ?>','n1_sol_<?php echo $i ; ?>');" type="text"></td>
<td><input id="n1_sol_<?php echo $i ; ?>"  value="<?php echo $val['n1_sol_'.$i.'']; ?>" 
<?php 
if (( $val['n1_sol_'.$i.''] >= $form_info['injected_solution'])&&( $val['n1_sol_'.$i.'']<= $form_info['injected_solution2']))
{echo "style='background-color:#aaddaa;'";}
else
{echo "style='background-color:#ffaaaa;'";}	
?> 
name="n1_sol_<?php echo $i ; ?>" onchange="minus('n1_etw_<?php echo $i ; ?>','n1_ftw_<?php echo $i ; ?>','n1_sol_<?php echo $i ; ?>');" type="text"></td>
<td rowspan="10"><select name="result<?php echo $i ; ?>" style="height: 30px;" ><option value="pass">pass</option><option value="fail" <?php if($val['result'.$i.'']=="fail"){echo "selected";} ?>>fail</option></select>&nbsp;&nbsp;</td>
</tr>

<tr>
<td>N2</td>
<td><input id="n2_etw_<?php echo $i ; ?>"  value="<?php echo $val['n2_etw_'.$i.'']; ?>" name="n2_etw_<?php echo $i ; ?>" onchange="minus('n2_etw_<?php echo $i ; ?>','n2_ftw_<?php echo $i ; ?>','n2_sol_<?php echo $i ; ?>');" type="text"></td>
<td><input id="n2_ftw_<?php echo $i ; ?>"  value="<?php echo $val['n2_ftw_'.$i.'']; ?>" name="n2_ftw_<?php echo $i ; ?>" onchange="minus('n2_etw_<?php echo $i ; ?>','n2_ftw_<?php echo $i ; ?>','n2_sol_<?php echo $i ; ?>');" type="text"></td>
<td><input id="n2_sol_<?php echo $i ; ?>"  value="<?php echo $val['n2_sol_'.$i.'']; ?>"
<?php 
if (( $val['n2_ftw_'.$i.''] >= $form_info['injected_solution'])&&( $val['n2_ftw_'.$i.'']<= $form_info['injected_solution2']))
{echo "style='background-color:#aaddaa;'";}
else
{echo "style='background-color:#ffaaaa;'";}	
?>
  name="n2_sol_<?php echo $i ; ?>" onchange="minus('n2_etw_<?php echo $i ; ?>','n2_ftw_<?php echo $i ; ?>','n2_sol_<?php echo $i ; ?>');" type="text"></td>
</tr>

<tr>
<td>N3</td>
<td><input id="n3_etw_<?php echo $i ; ?>"  value="<?php echo $val['n3_etw_'.$i.'']; ?>" name="n3_etw_<?php echo $i ; ?>" onchange="minus('n3_etw_<?php echo $i ; ?>','n3_ftw_<?php echo $i ; ?>','n3_sol_<?php echo $i ; ?>');" type="text"></td>
<td><input id="n3_ftw_<?php echo $i ; ?>"  value="<?php echo $val['n3_ftw_'.$i.'']; ?>" name="n3_ftw_<?php echo $i ; ?>" onchange="minus('n3_etw_<?php echo $i ; ?>','n3_ftw_<?php echo $i ; ?>','n3_sol_<?php echo $i ; ?>');" type="text"></td>
<td><input id="n3_sol_<?php echo $i ; ?>"  value="<?php echo $val['n3_sol_'.$i.'']; ?>" 
<?php 
if (( $val['n3_sol_'.$i.''] >= $form_info['injected_solution'])&&( $val['n3_sol_'.$i.'']<= $form_info['injected_solution2']))
{echo "style='background-color:#aaddaa;'";}
else
{echo "style='background-color:#ffaaaa;'";}	
?> 
name="n3_sol_<?php echo $i ; ?>" onchange="minus('n3_etw_<?php echo $i ; ?>','n3_ftw_<?php echo $i ; ?>','n3_sol_<?php echo $i ; ?>');" type="text"></td>
</tr>

<tr>
<td>N4</td>
<td><input id="n4_etw_<?php echo $i ; ?>"  value="<?php echo $val['n4_etw_'.$i.'']; ?>" name="n4_etw_<?php echo $i ; ?>" onchange="minus('n4_etw_<?php echo $i ; ?>','n4_ftw_<?php echo $i ; ?>','n4_sol_<?php echo $i ; ?>');" type="text"></td>
<td><input id="n4_ftw_<?php echo $i ; ?>"  value="<?php echo $val['n4_ftw_'.$i.'']; ?>" name="n4_ftw_<?php echo $i ; ?>" onchange="minus('n4_etw_<?php echo $i ; ?>','n4_ftw_<?php echo $i ; ?>','n4_sol_<?php echo $i ; ?>');" type="text"></td>
<td><input id="n4_sol_<?php echo $i ; ?>"  value="<?php echo $val['n4_sol_'.$i.'']; ?>"
<?php 
if (( $val['n4_sol_'.$i.''] >= $form_info['injected_solution'])&&( $val['n4_sol_'.$i.'']<= $form_info['injected_solution2']))
{echo "style='background-color:#aaddaa;'";}
else
{echo "style='background-color:#ffaaaa;'";}	
?> 
 name="n4_sol_<?php echo $i ; ?>" onchange="minus('n4_etw_<?php echo $i ; ?>','n4_ftw_<?php echo $i ; ?>','n4_sol_<?php echo $i ; ?>');" type="text"></td>
</tr>

<tr>
<td>N5</td>
<td><input id="n5_etw_<?php echo $i ; ?>"  value="<?php echo $val['n5_etw_'.$i.'']; ?>" name="n5_etw_<?php echo $i ; ?>" onchange="minus('n5_etw_<?php echo $i ; ?>','n5_ftw_<?php echo $i ; ?>','n5_sol_<?php echo $i ; ?>');" type="text"></td>
<td><input id="n5_ftw_<?php echo $i ; ?>"  value="<?php echo $val['n5_ftw_'.$i.'']; ?>" name="n5_ftw_<?php echo $i ; ?>" onchange="minus('n5_etw_<?php echo $i ; ?>','n5_ftw_<?php echo $i ; ?>','n5_sol_<?php echo $i ; ?>');" type="text"></td>
<td><input id="n5_sol_<?php echo $i ; ?>"  value="<?php echo $val['n5_sol_'.$i.'']; ?>" 
<?php 
if (( $val['n5_sol_'.$i.''] >= $form_info['injected_solution'])&&( $val['n5_sol_'.$i.'']<= $form_info['injected_solution2']))
{echo "style='background-color:#aaddaa;'";}
else
{echo "style='background-color:#ffaaaa;'";}	
?> 
name="n5_sol_<?php echo $i ; ?>" onchange="minus('n5_etw_<?php echo $i ; ?>','n5_ftw_<?php echo $i ; ?>','n5_sol_<?php echo $i ; ?>');" type="text"></td>
</tr>

<tr>
<td>N6</td>
<td><input id="n6_etw_<?php echo $i ; ?>"  value="<?php echo $val['n6_etw_'.$i.'']; ?>" name="n6_etw_<?php echo $i ; ?>" onchange="minus('n6_etw_<?php echo $i ; ?>','n6_ftw_<?php echo $i ; ?>','n6_sol_<?php echo $i ; ?>');" type="text"></td>
<td><input id="n6_ftw_<?php echo $i ; ?>"  value="<?php echo $val['n6_ftw_'.$i.'']; ?>"  name="n6_ftw_<?php echo $i ; ?>" onchange="minus('n6_etw_<?php echo $i ; ?>','n6_ftw_<?php echo $i ; ?>','n6_sol_<?php echo $i ; ?>');" type="text"></td>
<td><input id="n6_sol_<?php echo $i ; ?>"  value="<?php echo $val['n6_sol_'.$i.'']; ?>" 
<?php 
if (( $val['n6_sol_'.$i.''] >= $form_info['injected_solution'])&&( $val['n6_sol_'.$i.'']<= $form_info['injected_solution2']))
{echo "style='background-color:#aaddaa;'";}
else
{echo "style='background-color:#ffaaaa;'";}	
?> 
name="n6_sol_<?php echo $i ; ?>" onchange="minus('n6_etw_<?php echo $i ; ?>','n6_ftw_<?php echo $i ; ?>','n6_sol_<?php echo $i ; ?>');" type="text"></td>
</tr>

<tr>
<td>N7</td>
<td><input id="n7_etw_<?php echo $i ; ?>"  value="<?php echo $val['n7_etw_'.$i.'']; ?>" name="n7_etw_<?php echo $i ; ?>" onchange="minus('n7_etw_<?php echo $i ; ?>','n7_ftw_<?php echo $i ; ?>','n7_sol_<?php echo $i ; ?>');" type="text"></td>
<td><input id="n7_ftw_<?php echo $i ; ?>"  value="<?php echo $val['n7_ftw_'.$i.'']; ?>" name="n7_ftw_<?php echo $i ; ?>" onchange="minus('n7_etw_<?php echo $i ; ?>','n7_ftw_<?php echo $i ; ?>','n7_sol_<?php echo $i ; ?>');" type="text"></td>
<td><input id="n7_sol_<?php echo $i ; ?>"  value="<?php echo $val['n7_sol_'.$i.'']; ?>" 
<?php 
if (( $val['n7_sol_'.$i.''] >= $form_info['injected_solution'])&&( $val['n7_sol_'.$i.'']<= $form_info['injected_solution2']))
{echo "style='background-color:#aaddaa;'";}
else
{echo "style='background-color:#ffaaaa;'";}	
?> 
name="n7_sol_<?php echo $i ; ?>" onchange="minus('n7_etw_<?php echo $i ; ?>','n7_ftw_<?php echo $i ; ?>','n7_sol_<?php echo $i ; ?>');" type="text"></td>
</tr>

<tr>
<td>N8</td>
<td><input id="n8_etw_<?php echo $i ; ?>"  value="<?php echo $val['n8_etw_'.$i.'']; ?>" name="n8_etw_<?php echo $i ; ?>" onchange="minus('n8_etw_<?php echo $i ; ?>','n8_ftw_<?php echo $i ; ?>','n8_sol_<?php echo $i ; ?>');" type="text"></td>
<td><input id="n8_ftw_<?php echo $i ; ?>"  value="<?php echo $val['n8_ftw_'.$i.'']; ?>" name="n8_ftw_<?php echo $i ; ?>" onchange="minus('n8_etw_<?php echo $i ; ?>','n8_ftw_<?php echo $i ; ?>','n8_sol_<?php echo $i ; ?>');" type="text"></td>
<td><input id="n8_sol_<?php echo $i ; ?>"  value="<?php echo $val['n8_sol_'.$i.'']; ?>" 
<?php 
if (( $val['n8_sol_'.$i.''] >= $form_info['injected_solution'])&&( $val['n8_sol_'.$i.'']<= $form_info['injected_solution2']))
{echo "style='background-color:#aaddaa;'";}
else
{echo "style='background-color:#ffaaaa;'";}	
?> 
name="n8_sol_<?php echo $i ; ?>" onchange="minus('n8_etw_<?php echo $i ; ?>','n8_ftw_<?php echo $i ; ?>','n8_sol_<?php echo $i ; ?>');" type="text"></td>
</tr>


<tr>
<td>N9</td>
<td><input id="n9_etw_<?php echo $i ; ?>"  value="<?php echo $val['n9_etw_'.$i.'']; ?>" name="n9_etw_<?php echo $i ; ?>" onchange="minus('n9_etw_<?php echo $i ; ?>','n9_ftw_<?php echo $i ; ?>','n9_sol_<?php echo $i ; ?>');" type="text"></td>
<td><input id="n9_ftw_<?php echo $i ; ?>"  value="<?php echo $val['n9_ftw_'.$i.'']; ?>" name="n9_ftw_<?php echo $i ; ?>" onchange="minus('n9_etw_<?php echo $i ; ?>','n9_ftw_<?php echo $i ; ?>','n9_sol_<?php echo $i ; ?>');" type="text"></td>
<td><input id="n9_sol_<?php echo $i ; ?>"  value="<?php echo $val['n9_sol_'.$i.'']; ?>"
<?php 
if (( $val['n9_sol_'.$i.''] >= $form_info['injected_solution'])&&( $val['n9_sol_'.$i.'']<= $form_info['injected_solution2']))
{echo "style='background-color:#aaddaa;'";}
else
{echo "style='background-color:#ffaaaa;'";}	
?> 
 name="n9_sol_<?php echo $i ; ?>" onchange="minus('n9_etw_<?php echo $i ; ?>','n9_ftw_<?php echo $i ; ?>','n9_sol_<?php echo $i ; ?>');" type="text"></td>
</tr>


<tr>
<td>N10</td>
<td><input id="n10_etw_<?php echo $i ; ?>"  value="<?php echo $val['n10_etw_'.$i.'']; ?>" name="n10_etw_<?php echo $i ; ?>" onchange="minus('n10_etw_<?php echo $i ; ?>','n10_ftw_<?php echo $i ; ?>','n10_sol_<?php echo $i ; ?>');" type="text"></td>
<td><input id="n10_ftw_<?php echo $i ; ?>"  value="<?php echo $val['n10_ftw_'.$i.'']; ?>" name="n10_ftw_<?php echo $i ; ?>" onchange="minus('n10_etw_<?php echo $i ; ?>','n10_ftw_<?php echo $i ; ?>','n10_sol_<?php echo $i ; ?>');" type="text"></td>
<td><input id="n10_sol_<?php echo $i ; ?>"  value="<?php echo $val['n10_sol_'.$i.'']; ?>"
<?php 
if (( $val['n10_sol_'.$i.''] >= $form_info['injected_solution'])&&( $val['n10_sol_'.$i.'']<= $form_info['injected_solution2']))
{echo "style='background-color:#aaddaa;'";}
else
{echo "style='background-color:#ffaaaa;'";}	
?> 
 name="n10_sol_<?php echo $i ; ?>" onchange="minus('n10_etw_<?php echo $i ; ?>','n10_ftw_<?php echo $i ; ?>','n10_sol_<?php echo $i ; ?>');" type="text"></td>
</tr>

</table>
</div>


<?php
$i ++ ;
} ; ?>

</div>



<hr style="border-bottom:2px solid #888;width: 100%;">
<div class="row">
<div class="col-xs-2"  style="padding-right:0px;">
<label>1.Solution weight</label><br>
<label>2.filled tube weight</label><br>
<label>3.empty tube weight</label>
</div>
<div class="col-xs-10">
<label style="width:30%;text-align:right;padding-right:10px;">Conclusion:</label><label style="width:70%;border: 1px solid #aaa;">Products were tested & results found complying with AFCO Specification</label><br>
<label style="width:30%;text-align:right;padding-right:10px;">Test completion Date/ Time:</label><input name="test_completion_date" value="<?php echo $form_info['test_completion_date'];?>" style="width:70%;font-size: 60%;" type="datetime-local"><br>
<label style="width:30%;text-align:right;padding-right:10px;">Tested By Sign & Date:</label><input name="tested_by" value="<?php echo $form_info['tested_by'];?>"  style="width:70%;font-size: 60%;" type="text"><br>
<label style="width:30%;text-align:right;padding-right:10px;">Approved By Sign & Date:</label><input name="Approved_By" value="<?php echo $form_info['Approved_By'];?>" type="text" style="width:70%;border: 1px solid #aaa;height: 17px;"><br>
</div>

</div>
<input type="hidden" id="count_forms" name="count_forms" value="<?php echo $form_info['count_forms'];?>">
<input name="id" type="hidden" value="<?php echo $id; ?>">
</div>
<div style="padding:20px 20px;text-align:right;">
<label class="btn btn-default" style="width:150px;border-radius:5px;height:30px;padding-top: 4px;" onclick="newForm()">Add New table</label>
<label class="btn btn-default" style="width:150px;border-radius:5px;height:30px;padding-top: 4px;" onclick="printContent('form')">Print Content</label>
<input type="submit" class="btn-success" style="width:150px;border-radius:5px;height:30px;" value="Save">
</div>
</form>

</body>
</html>

<script>
function minus(x,y,z)
{
var x = jQuery('#'+x).val();
var y = jQuery('#'+y).val();

x = parseFloat(x) ; 
y = parseFloat(y) ; 

var sum = parseFloat(y* 10000000 - x* 10000000).toFixed( 2 )/10000000;
 //sum= (sum  / 10;
console.log(sum);
if (sum)
{
jQuery('#'+z).val(sum);
var x2 = jQuery('#injected_solution').val();
var y2 = jQuery('#injected_solution2').val();
x2 = parseFloat(x2 * 10000000).toFixed( 2 )/10000000 ; 
y2 = parseFloat(y2 * 10000000).toFixed( 2 )/10000000;
if((x2 <= sum )&&(sum <= y2)) 
{
	 jQuery('#'+z).css("background-color","#aaddaa");
}
else
{
	 jQuery('#'+z).css("background-color","#ffaaaa");
}

}
	
}

function printContent(el){
	var restorepage = document.body.innerHTML;
	var printcontent = document.getElementById(el).innerHTML;
	document.body.innerHTML = printcontent;
	window.print();
	document.body.innerHTML = restorepage;
}


function newForm()
{
var numItems = $('.dawood').length ;
numItems ++ ;

var form = "";
form +='<div class="col-xs-6 dawood">';
form +='<table style="width:100%;margin-bottom: 20px;">';
form +='<tr><td  rowspan="3">Tube NO</td><td colspan="4" >Test result at start up</td></tr>';
form +='<tr><td colspan="4" ><input name="test_date'+numItems+'" style="width:100%;" type="datetime-local"></td></tr>';

form +='<tr>';
form +='<td>ETW</td>';
form +='<td>FTW</td>';
form +='<td>SOL-W</td>';
form +='<td >&nbsp;&nbsp;result&nbsp;&nbsp;</td>';
form +='</tr>';

form +='<tr>';
form +='<td>N1</td>';
form +='<td><input id="n1_etw_'+numItems+'" name="n1_etw_'+numItems+'" onchange="minus(\'n1_etw_'+numItems+'\',\'n1_ftw_'+numItems+'\',\'n1_sol_'+numItems+'\');" type="text"></td>';
form +='<td><input id="n1_ftw_'+numItems+'" name="n1_ftw_'+numItems+'" onchange="minus(\'n1_etw_'+numItems+'\',\'n1_ftw_'+numItems+'\',\'n1_sol_'+numItems+'\');" type="text"></td>';
form +='<td><input id="n1_sol_'+numItems+'" name="n1_sol_'+numItems+'" onchange="minus(\'n1_etw_'+numItems+'\',\'n1_ftw_'+numItems+'\',\'n1_sol_'+numItems+'\');" type="text"></td>';
form +='<td rowspan="10"><select  name="result'+numItems+'"  style="height: 30px;" ><option value="pass">pass</option><option value="fail">fail</option></select>&nbsp;&nbsp;</td>';
form +='</tr>';

form +='<tr>';
form +='<td>N2</td>';
form +='<td><input id="n2_etw_'+numItems+'" name="n2_etw_'+numItems+'" onchange="minus(\'n2_etw_'+numItems+'\',\'n2_ftw_'+numItems+'\',\'n2_sol_'+numItems+'\');" type="text"></td>';
form +='<td><input id="n2_ftw_'+numItems+'" name="n2_ftw_'+numItems+'" onchange="minus(\'n2_etw_'+numItems+'\',\'n2_ftw_'+numItems+'\',\'n2_sol_'+numItems+'\');" type="text"></td>';
form +='<td><input id="n2_sol_'+numItems+'" name="n2_sol_'+numItems+'" onchange="minus(\'n2_etw_'+numItems+'\',\'n2_ftw_'+numItems+'\',"n2_sol_'+numItems+'\');" type="text"></td>';
form +='</tr>';

form +='<tr>';
form +='<td>N3</td>';
form +='<td><input id="n3_etw_'+numItems+'" name="n3_etw_'+numItems+'" onchange="minus(\'n3_etw_'+numItems+'\',\'n3_ftw_'+numItems+'\',\'n3_sol_'+numItems+'\');" type="text"></td>';
form +='<td><input id="n3_ftw_'+numItems+'" name="n3_ftw_'+numItems+'" onchange="minus(\'n3_etw_'+numItems+'\',\'n3_ftw_'+numItems+'\',\'n3_sol_'+numItems+'\');" type="text"></td>';
form +='<td><input id="n3_sol_'+numItems+'" name="n3_sol_'+numItems+'" onchange="minus(\'n3_etw_'+numItems+'\',\'n3_ftw_'+numItems+'\',\'n3_sol_'+numItems+'\');" type="text"></td>';
form +='</tr>';

form +='<tr>';
form +='<td>N4</td>';
form +='<td><input id="n4_etw_'+numItems+'" name="n4_etw_'+numItems+'" onchange="minus(\'n4_etw_'+numItems+'\',\'n4_ftw_'+numItems+'\',\'n4_sol_'+numItems+'\');" type="text"></td>';
form +='<td><input id="n4_ftw_'+numItems+'" name="n4_ftw_'+numItems+'" onchange="minus(\'n4_etw_'+numItems+'\',\'n4_ftw_'+numItems+'\',\'n4_sol_'+numItems+'\');" type="text"></td>';
form +='<td><input id="n4_sol_'+numItems+'" name="n4_sol_'+numItems+'" onchange="minus(\'n4_etw_'+numItems+'\',\'n4_ftw_'+numItems+'\',\'n4_sol_'+numItems+'\');" type="text"></td>';
form +='</tr>';

form +='<tr>';
form +='<td>N5</td>';
form +='<td><input id="n5_etw_'+numItems+'" name="n5_etw_'+numItems+'" onchange="minus(\'n5_etw_'+numItems+'\',\'n5_ftw_'+numItems+'\',\'n5_sol_'+numItems+'\');" type="text"></td>';
form +='<td><input id="n5_ftw_'+numItems+'" name="n5_ftw_'+numItems+'" onchange="minus(\'n5_etw_'+numItems+'\',\'n5_ftw_'+numItems+'\',\'n5_sol_'+numItems+'\');" type="text"></td>';
form +='<td><input id="n5_sol_'+numItems+'" name="n5_sol_'+numItems+'" onchange="minus(\'n5_etw_'+numItems+'\',\'n5_ftw_'+numItems+'\',\'n5_sol_'+numItems+'\');" type="text"></td>';
form +='</tr>';

form +='<tr>';
form +='<td>N6</td>';
form +='<td><input id="n6_etw_'+numItems+'" name="n6_etw_'+numItems+'" onchange="minus(\'n6_etw_'+numItems+'\',\'n6_ftw_'+numItems+'\',\'n6_sol_'+numItems+'\');" type="text"></td>';
form +='<td><input id="n6_ftw_'+numItems+'" name="n6_ftw_'+numItems+'" onchange="minus(\'n6_etw_'+numItems+'\',\'n6_ftw_'+numItems+'\',\'n6_sol_'+numItems+'\');" type="text"></td>';
form +='<td><input id="n6_sol_'+numItems+'" name="n6_sol_'+numItems+'" onchange="minus(\'n6_etw_'+numItems+'\',\'n6_ftw_'+numItems+'\',\'n6_sol_'+numItems+'\');" type="text"></td>';
form +='</tr>';

form +='<tr>';
form +='<td>N7</td>';
form +='<td><input id="n7_etw_'+numItems+'" name="n7_etw_'+numItems+'" onchange="minus(\'n7_etw_'+numItems+'\',\'n7_ftw_'+numItems+'\',\'n7_sol_'+numItems+'\');" type="text"></td>';
form +='<td><input id="n7_ftw_'+numItems+'" name="n7_ftw_'+numItems+'" onchange="minus(\'n7_etw_'+numItems+'\',\'n7_ftw_'+numItems+'\',\'n7_sol_'+numItems+'\');" type="text"></td>';
form +='<td><input id="n7_sol_'+numItems+'" name="n7_sol_'+numItems+'" onchange="minus(\'n7_etw_'+numItems+'\',\'n7_ftw_'+numItems+'\',\'n7_sol_'+numItems+'\');" type="text"></td>';
form +='</tr>';

form +='<tr>';
form +='<td>N8</td>';
form +='<td><input id="n8_etw_'+numItems+'" name="n8_etw_'+numItems+'" onchange="minus(\'n8_etw_'+numItems+'\',\'n8_ftw_'+numItems+'\',\'n8_sol_'+numItems+'\');" type="text"></td>';
form +='<td><input id="n8_ftw_'+numItems+'" name="n8_ftw_'+numItems+'" onchange="minus(\'n8_etw_'+numItems+'\',\'n8_ftw_'+numItems+'\',\'n8_sol_'+numItems+'\');" type="text"></td>';
form +='<td><input id="n8_sol_'+numItems+'" name="n8_sol_'+numItems+'" onchange="minus(\'n8_etw_'+numItems+'\',\'n8_ftw_'+numItems+'\',\'n8_sol_'+numItems+'\');" type="text"></td>';
form +='</tr>';


form +='<tr>';
form +='<td>N9</td>';
form +='<td><input id="n9_etw_'+numItems+'" name="n9_etw_'+numItems+'" onchange="minus(\'n9_etw_'+numItems+'\',\'n9_ftw_'+numItems+'\',\'n9_sol_'+numItems+'\');" type="text"></td>';
form +='<td><input id="n9_ftw_'+numItems+'" name="n9_ftw_'+numItems+'" onchange="minus(\'n9_etw_'+numItems+'\',\'n9_ftw_'+numItems+'\',\'n9_sol_'+numItems+'\');" type="text"></td>';
form +='<td><input id="n9_sol_'+numItems+'" name="n9_sol_'+numItems+'" onchange="minus(\'n9_etw_'+numItems+'\',\'n9_ftw_'+numItems+'\',\'n9_sol_'+numItems+'\');" type="text"></td>';
form +='</tr>';


form +='<tr>';
form +='<td>N10</td>';
form +='<td><input id="n10_etw_'+numItems+'" name="n10_etw_'+numItems+'" onchange="minus(\'n10_etw_'+numItems+'\',\'n10_ftw_'+numItems+'\',\'n10_sol_'+numItems+'\');" type="text"></td>';
form +='<td><input id="n10_ftw_'+numItems+'" name="n10_ftw_'+numItems+'" onchange="minus(\'n10_etw_'+numItems+'\',\'n10_ftw_'+numItems+'\',\'n10_sol_'+numItems+'\');" type="text"></td>';
form +='<td><input id="n10_sol_'+numItems+'" name="n10_sol_'+numItems+'" onchange="minus(\'n10_etw_'+numItems+'\',\'n10_ftw_'+numItems+'\',\'n10_sol_'+numItems+'\');" type="text"></td>';
form +='</tr>';

form +='</table>';
form +='</div>';


if (numItems == 5 || numItems == 9 || numItems == 13 || numItems == 17 || numItems == 21 || numItems == 25 || numItems == 29)
{
    $('td').css("font-size","98%");
    $('td input').css("font-size","98%");
	var	form2 ='';
	if(numItems >=9 )
	{
		form2+='<div class="col-xs-12 show-header" style="height:181px;"></div>';
	}
     //form2+='<div class="col-xs-12" style="height:110px;"></div>';
	    form2 += '<div class="col-xs-12 show-header">'; 
	      form2 += '<hr style="border-bottom:2px solid #888;width: 100%;">';
		form2 += '<div class="col-xs-4">';
	form2 += '<img src="../afco/images/acfo_logo.png" style="width:100%;">';
		form2 += '</div>';

		form2 += '<div class="col-xs-4" style="text-align:center;">';
		form2 += '<br>';
		form2 += '<label>AL Hanoof Factory for Medical & lab Supplies</label><br>';
		form2 += '<label>Weighing of injected solutions test report</label><br>';
	form2 += '</div>';

	   form2 += '<div class="col-xs-4" >';
		form2 += '<div class="row" style="border-bottom:1px solid #888;font-size: 65%;">';
		form2 += '<div class="col-xs-4">Doc No:</div>';
		form2 += '<div class="col-xs-8"><input name="doc_no" value="FOR-QCD-037" readonly style="width:100%;" type="text"></div>';
		form2 += '</div>';

		form2 += '<div class="row" style="border-bottom:1px solid #888;font-size: 65%;">';
		form2 += '<div class="col-xs-4">Issue Date:</div>';
		form2 += '<div class="col-xs-8"><input  name="issue_date" style="width:100%;" readonly value="2016-08-11" type="date"></div>';
		form2 += '</div>';

		form2 += '<div class="row" style="border-bottom:1px solid #888;font-size: 65%;">';
		form2 += '<div class="col-xs-4">Effective Date:</div>';
		form2 += '<div class="col-xs-8"><input  name="effictive_date" style="width:100%;" value="2016-08-11" readonly type="date"></div>';
		form2 += '</div>';

		form2 += '<div class="row" style="border-bottom:1px solid #888;font-size: 65%;">';
		form2 += '<div class="col-xs-4">Rev No:</div>';
		form2 += '<div class="col-xs-8"> <input  name="rev_no" style="width:100%;" value="00" readonly type="text"></div>';
		form2 += '</div>';

		form2 += '<div class="row" style="border-bottom:1px solid #888;font-size: 65%;">';
		form2 += '<div class="col-xs-4">Next Rev Date:</div>';
		form2 += '<div class="col-xs-8"> <input  name="next_rev_date" value="2019-08-11" readonly style="width:100%;" type="date"></div>';
		form2 += '</div>';


		form2 += '<div class="row" style="font-size: 65%;">';
		form2 += '<div class="col-xs-4">Supersede Date:</div>';
		form2 += '<div class="col-xs-8"> <input  name="supersede_date" value="NA" readonly style="width:100%;" type="text"></div>';
		form2 += '</div>';

	  form2 += '</div>';
    form2 += '<hr style="border-bottom:2px solid #888;width: 100%;">';
  form2 += '</div>';
}
else{var form2='';}
$('#forms').append(form2);

$('#forms').append(form);
$('#count_forms').val(numItems);
}
</script>
