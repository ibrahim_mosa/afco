<!DOCTYPE html>
<html>
<head>
  <title>Afco</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/font-awesome.min.css">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <style>
  td{border:1px solid #888;text-align:center;font-size: 97%;}
  td input{width:80%;text-align:center;font-size: 98%;}
  label{font-size:75%;margin-bottom:0px;}
  hr{margin-top:5px!important;margin-bottom:5px!important;}
  div select{font-size: 75%;}
  </style>
</head>
<body style="background-color:#eee;">
<form action="../afco/save.php" method="post" style="padding:15px;">
<div class="row" id="form" style="border:2px solid #888;margin-right: 10px;margin-left: 10px;padding: 5px;background-color:#fff;position:relative;">
    <div class="">
		<div class="col-xs-4">
			<img src="../afco/images/acfo_logo.png" style="width:100%;">
		</div>

		<div class="col-xs-4" style="text-align:center;">
			<br>
			<label>AL Hanoof Factory for Medical & lab Supplies</label><br>
			<label>Weighing of injected solutions test report</label><br>
		</div>

	   <div class="col-xs-4" >
		<div class="row" style="border-bottom:1px solid #888;font-size: 65%;">
			<div class="col-xs-4">Doc No:</div>
			<div class="col-xs-8"><input name="doc_no" value="FOR-QCD-037" readonly style="width:100%;" type="text"></div>
		</div>

		<div class="row" style="border-bottom:1px solid #888;font-size: 65%;">
			<div class="col-xs-4">Issue Date:</div>
			<div class="col-xs-8"><input  name="issue_date" style="width:100%;" readonly value="2016-08-11" type="date"></div>
		</div>

		<div class="row" style="border-bottom:1px solid #888;font-size: 65%;">
			<div class="col-xs-4">Effective Date:</div>
			<div class="col-xs-8"><input  name="effictive_date" style="width:100%;" value="2016-08-11" readonly type="date"></div>
		</div>

		<div class="row" style="border-bottom:1px solid #888;font-size: 65%;">
			<div class="col-xs-4">Rev No:</div>
			<div class="col-xs-8"> <input  name="rev_no" style="width:100%;" value="00" readonly type="text"></div>
		</div>

		<div class="row" style="border-bottom:1px solid #888;font-size: 65%;">
			<div class="col-xs-4">Next Rev Date:</div>
			<div class="col-xs-8"> <input  name="next_rev_date" value="2019-08-11" readonly style="width:100%;" type="date"></div>
		</div>


		<div class="row" style="font-size: 65%;">
			<div class="col-xs-4">Supersede Date:</div>
			<div class="col-xs-8"> <input  name="supersede_date" value="NA" readonly style="width:100%;" type="text"></div>
		</div>

	   </div>
    </div>
<hr style="border-bottom:2px solid #888;width: 100%;">

<div class="row">
<div class="col-xs-3"><label  style="width:100%;">Test Starting Date/Time:</label></div>
<div class="col-xs-6"><label  style="width:100%;"><input  name="test_start_date" style="width:90%;" type="datetime-local"></label></div><br>
</div>

<div class="row">
<div class="col-xs-3"><label  style="width:100%;">Product Name:</label></div>
<div class="col-xs-6"><label  style="width:100%;"><input  name="product_name" style="width:90%;" type="text"></label></div>
<div class="col-xs-1" style="text-align: right;"><label  style="width:100%;">Batch No:</label></div>
<div class="col-xs-2"><label  style="width:100%;"><input  name="batch_no" style="width:90%;" type="text"></label></div><br>
</div>

<div class="row">
<div class="col-xs-3"><label  style="width:100%;">Batch size:</label></div>
<div class="col-xs-6"><label  style="width:100%;"><input  name="batch_size" style="width:90%;" type="text"></label></div>
<div class="col-xs-1" style="text-align: right;"><label  style="width:100%;">Cat No:</label></div>
<div class="col-xs-2"><label  style="width:100%;"><input  name="cat_no" style="width:90%;" type="text"></label></div><br>
</div>

<div class="row">
<div class="col-xs-3"><label  style="width:100%;">Expire Date:</label></div>
<div class="col-xs-6"><label  style="width:100%;"><input  name="expire_date" style="width:90%;" type="month"></label></div><br>
</div>

<div class="row">
<div class="col-xs-3"><label  style="width:100%;">Injected solution acceptance tolerance:</label></div>
<div class="col-xs-6"><label  style="width:100%;"><input  name="injected_solution" id="injected_solution" style="width:20%;" type="text">&nbsp;<label style="width:25%">gm. to</label>
<input  name="injected_solution2" id="injected_solution2" style="width:20%;" type="text">&nbsp;<label style="width:25%"> gm.</label>
</label></div><br>
</div>

<div class="row">
<div class="col-xs-3"><label  style="width:100%;">Used balancename:</label></div>
<div class="col-xs-6">
	<select id="id_no1-1" onchange="changeID($(this).children(':selected').attr('id'))" name="id_no1-1" style="width: 90%;margin-top: 1px;">
     <option id="1" value="1">ADAM CBK8 H Balance, Serial No.: AE682961</option> 
     <option id="2" value="2">ADAM CBK8 H Balance, Serial No.: AE6821711</option> 
     <option id="3" value="3">ADAM CBK8 H Balance, Serial No.: AE6829659</option> 
     <option id="4" value="4">ADAM CBK4 Balance, Serial No.: AE5961859</option> 
     <option id="5" value="5">ADAM CBK4 Balance, Serial No.: AE5961868</option> 
     <option id="6" value="6">ADAM CBK4 Balance</option> 
     <option id="7" value="7">EQ-120 Balance, Serial No.: 19304712</option> 
     <option id="8" value="8">Denever TP-303 Balance, , Serial No.: 26208774</option> 
     <option id="9" value="9">AE ADAM Balance, Serial No.: AE30L1402.</option> 
	</select><!--<label  style="width: 90%;border: 1px solid #aaa;"> Adam balance </label>-->
</div>
<div class="col-xs-1" style="text-align: right;"><label  style="width:100%;">ID No:</label></div>
<div class="col-xs-2">
    <select id="id_no1" name="id_no1" onchange="changeBalancename($(this).children(':selected').attr('id'))" style="width:90%;margin-top: 1px;" >
     <option id="1" value="1">BAL-001</option> 
     <option id="2" value="2">BAL-002</option> 
     <option id="3" value="3">BAL-003</option> 
     <option id="4" value="4">BAL-004</option> 
     <option id="5" value="5">BAL-005</option> 
     <option id="6" value="6">BAL-006</option> 
     <option id="7" value="7">BAL-007</option> 
     <option id="8" value="8">BAL-008</option> 
     <option id="9" value="9">BAL-009</option> 
	</select>
</div><br>
</div>
		<script>
		function changeID(xx)
         { 
            $("#id_no1").val(xx);
		 }     
 
         function changeBalancename(yy)
         {
             $("#id_no1-1").val(yy);
         }
 
         function changeID2(xx)
         { 
            $("#id_no2").val(xx);
		 }

         function changeMachine(yy)
         {
             $("#id_no2-1").val(yy);
         }
		</script>
<div class="row">
<div class="col-xs-3"><label  style="width:100%;">Used filling machine name:</label></div>
<div class="col-xs-6">
	<select id="id_no2-1" onchange="changeID2($(this).children(':selected').attr('id'))" name="id_no2-1" style="width: 90%;margin-top: 1px;">
     <option id="1" value="1">Precision spraying Machine</option> 
     <option id="2" value="2">AXT-KT10 Automatic Adjustable Quantitative Spray & Filling System.</option> 
	</select>
</div>
<div class="col-xs-1" style="text-align: right;"><label  style="width:100%;">ID No:</label></div>
<div class="col-xs-2">
    <select id="id_no2" name="id_no2"  onchange="changeMachine($(this).children(':selected').attr('id'))" style="width:90%;margin-top: 1px;" >
     <option id="1" value="1">PRO2-017</option> 
     <option id="2" value="2">PRO2-004</option> 
	</select></div><br>
</div>

<hr  style="border-bottom:2px solid #888;width: 100%;">

<div class="row" id="forms">

<div class="col-xs-6 dawood" >
<table style="width:100%;margin-bottom: 20px;">
<tr><td rowspan="3">Tube NO</td><td colspan="4" >Test result at start up</td></tr>
<tr><td>Date & Time:</td><td colspan="3" ><input name="test_date1" style="width:100%;" type="datetime-local"></td></tr> 

<tr>

<td>ETW<span style="font-size: 75%;vertical-align: top;">3</span></td>
<td>FTW<span  style="font-size: 75%;vertical-align: top;">2</span></td>
<td>SOL-W<span  style="font-size: 75%;vertical-align: top;">1</span></td>
<td >&nbsp;&nbsp;result&nbsp;&nbsp;</td>
</tr>

<tr>
<td>N1</td>
<td><input id="n1_etw_1" name="n1_etw_1" onchange="minus('n1_etw_1','n1_ftw_1','n1_sol_1');" type="text"></td>
<td><input id="n1_ftw_1" name="n1_ftw_1" onchange="minus('n1_etw_1','n1_ftw_1','n1_sol_1');" type="text"></td>
<td><input id="n1_sol_1" name="n1_sol_1" onchange="minus('n1_etw_1','n1_ftw_1','n1_sol_1');" type="text"></td>
<td rowspan="10"><select name="result1" style="height: 30px;" ><option value="pass">pass</option><option value="fail">fail</option></select>&nbsp;&nbsp;</td>
</tr>

<tr>
<td>N2</td>
<td><input id="n2_etw_1" name="n2_etw_1" onchange="minus('n2_etw_1','n2_ftw_1','n2_sol_1');" type="text"></td>
<td><input id="n2_ftw_1" name="n2_ftw_1" onchange="minus('n2_etw_1','n2_ftw_1','n2_sol_1');" type="text"></td>
<td><input id="n2_sol_1" name="n2_sol_1" onchange="minus('n2_etw_1','n2_ftw_1','n2_sol_1');" type="text"></td>
</tr>

<tr>
<td>N3</td>
<td><input id="n3_etw_1" name="n3_etw_1" onchange="minus('n3_etw_1','n3_ftw_1','n3_sol_1');" type="text"></td>
<td><input id="n3_ftw_1" name="n3_ftw_1" onchange="minus('n3_etw_1','n3_ftw_1','n3_sol_1');" type="text"></td>
<td><input id="n3_sol_1" name="n3_sol_1" onchange="minus('n3_etw_1','n3_ftw_1','n3_sol_1');" type="text"></td>
</tr>

<tr>
<td>N4</td>
<td><input id="n4_etw_1" name="n4_etw_1" onchange="minus('n4_etw_1','n4_ftw_1','n4_sol_1');" type="text"></td>
<td><input id="n4_ftw_1" name="n4_ftw_1" onchange="minus('n4_etw_1','n4_ftw_1','n4_sol_1');" type="text"></td>
<td><input id="n4_sol_1" name="n4_sol_1" onchange="minus('n4_etw_1','n4_ftw_1','n4_sol_1');" type="text"></td>
</tr>

<tr>
<td>N5</td>
<td><input id="n5_etw_1" name="n5_etw_1" onchange="minus('n5_etw_1','n5_ftw_1','n5_sol_1');" type="text"></td>
<td><input id="n5_ftw_1" name="n5_ftw_1" onchange="minus('n5_etw_1','n5_ftw_1','n5_sol_1');" type="text"></td>
<td><input id="n5_sol_1" name="n5_sol_1" onchange="minus('n5_etw_1','n5_ftw_1','n5_sol_1');" type="text"></td>
</tr>

<tr>
<td>N6</td>
<td><input id="n6_etw_1" name="n6_etw_1" onchange="minus('n6_etw_1','n6_ftw_1','n6_sol_1');" type="text"></td>
<td><input id="n6_ftw_1" name="n6_ftw_1" onchange="minus('n6_etw_1','n6_ftw_1','n6_sol_1');" type="text"></td>
<td><input id="n6_sol_1" name="n6_sol_1" onchange="minus('n6_etw_1','n6_ftw_1','n6_sol_1');" type="text"></td>
</tr>

<tr>
<td>N7</td>
<td><input id="n7_etw_1" name="n7_etw_1" onchange="minus('n7_etw_1','n7_ftw_1','n7_sol_1');" type="text"></td>
<td><input id="n7_ftw_1" name="n7_ftw_1" onchange="minus('n7_etw_1','n7_ftw_1','n7_sol_1');" type="text"></td>
<td><input id="n7_sol_1" name="n7_sol_1" onchange="minus('n7_etw_1','n7_ftw_1','n7_sol_1');" type="text"></td>
</tr>

<tr>
<td>N8</td>
<td><input id="n8_etw_1" name="n8_etw_1" onchange="minus('n8_etw_1','n8_ftw_1','n8_sol_1');" type="text"></td>
<td><input id="n8_ftw_1" name="n8_ftw_1" onchange="minus('n8_etw_1','n8_ftw_1','n8_sol_1');" type="text"></td>
<td><input id="n8_sol_1" name="n8_sol_1" onchange="minus('n8_etw_1','n8_ftw_1','n8_sol_1');" type="text"></td>
</tr>


<tr>
<td>N9</td>
<td><input id="n9_etw_1" name="n9_etw_1" onchange="minus('n9_etw_1','n9_ftw_1','n9_sol_1');" type="text"></td>
<td><input id="n9_ftw_1" name="n9_ftw_1" onchange="minus('n9_etw_1','n9_ftw_1','n9_sol_1');" type="text"></td>
<td><input id="n9_sol_1" name="n9_sol_1" onchange="minus('n9_etw_1','n9_ftw_1','n9_sol_1');" type="text"></td>
</tr>


<tr>
<td>N10</td>
<td><input id="n10_etw_1" name="n10_etw_1" onchange="minus('n10_etw_1','n10_ftw_1','n10_sol_1');" type="text"></td>
<td><input id="n10_ftw_1" name="n10_ftw_1" onchange="minus('n10_etw_1','n10_ftw_1','n10_sol_1');" type="text"></td>
<td><input id="n10_sol_1" name="n10_sol_1" onchange="minus('n10_etw_1','n10_ftw_1','n10_sol_1');" type="text"></td>
</tr>

</table>
</div>


</div>

<hr style="border-bottom:2px solid #888;width: 100%;">
<div class="row">
<div class="col-xs-2" style="padding-right:0px;">
<label>1.Solution weight</label><br>
<label>2.filled tube weight</label><br>
<label>3.empty tube weight</label>
</div>
<div class="col-xs-10">
<label style="width:30%;text-align:right;padding-right:10px;">Conclusion:</label><label style="width:70%;border: 1px solid #aaa;">Products were tested & results found complying with AFCO Specification</label><br>
<label style="width:30%;text-align:right;padding-right:10px;">Test completion Date/ Time:</label><input name="test_completion_date" style="width:70%;font-size: 60%;" type="datetime-local"><br>
<label style="width:30%;text-align:right;padding-right:10px;">Tested By Sign & Date:</label><input name="tested_by" style="width:70%;font-size: 60%;" type="text"><br>
<label style="width:30%;text-align:right;padding-right:10px;">Approved By Sign & Date:</label><input name="Approved_By" type="text" style="width:70%;border: 1px solid #aaa;height: 17px;"><br>
</div>

</div>

<input type="hidden" id="count_forms" name="count_forms" value="1">
</div>
<div style="padding:20px 20px;text-align:right;">
<label class="btn btn-default" style="width:150px;border-radius:5px;height:30px;padding-top: 4px;" onclick="newForm()">Add New Table</label>
<!--<label class="btn btn-default" style="width:150px;border-radius:5px;height:30px;padding-top: 4px;" onclick="printContent('form')">Print Content</label>-->
<input type="submit" class="btn-success" style="width:150px;border-radius:5px;height:30px;" value="Save">
</div>
</form>
</body>
</html>

<script>
function minus(x,y,z)
{
var x = jQuery('#'+x).val();
var y = jQuery('#'+y).val();

x = parseFloat(x) ; 
y = parseFloat(y) ; 

var sum = parseFloat(y* 10000000 - x* 10000000).toFixed( 2 )/10000000;
 //sum= (sum  / 10;
console.log(sum);
if (sum)
{
jQuery('#'+z).val(sum);
var x2 = jQuery('#injected_solution').val();
var y2 = jQuery('#injected_solution2').val();
x2 = parseFloat(x2 * 10000000).toFixed( 2 )/10000000; 
y2 = parseFloat(y2 * 10000000).toFixed( 2 )/10000000;
if((x2 <= sum )&&(sum <= y2)) 
{
	 jQuery('#'+z).css("background-color","#aaddaa");
}
else
{
	 jQuery('#'+z).css("background-color","#ffaaaa");
}

}
	
}



function printContent(el){
	var restorepage = document.body.innerHTML;
	var printcontent = document.getElementById(el).innerHTML;
	document.body.innerHTML = printcontent;
	window.print();
	document.body.innerHTML = restorepage;
}


function newForm()
{
var numItems = $('.dawood').length ;
numItems ++ ;

var form = "";
form +='<div class="col-xs-6 dawood">';
form +='<table style="width:100%;margin-bottom: 20px;">';
form +='<tr><td rowspan="3">Tube NO</td><td colspan="4" >Test Result At:</td></tr>';
form +='<tr><td>Date & Time:</td><td colspan="3" ><input name="test_date'+numItems+'" style="width:100%;" type="datetime-local"></td></tr>';

form +='<tr>';
form +='<td>ETW<span style="font-size: 75%;vertical-align: top;">3</span></td>';
form +='<td>FTW<span style="font-size: 75%;vertical-align: top;">2</span></td>';
form +='<td>SOL-W<span style="font-size: 75%;vertical-align: top;">1</span></td>';
form +='<td >&nbsp;&nbsp;result&nbsp;&nbsp;</td>';
form +='</tr>';

form +='<tr>';
form +='<td>N1</td>';
form +='<td><input id="n1_etw_'+numItems+'" name="n1_etw_'+numItems+'" onchange="minus(\'n1_etw_'+numItems+'\',\'n1_ftw_'+numItems+'\',\'n1_sol_'+numItems+'\');" type="text"></td>';
form +='<td><input id="n1_ftw_'+numItems+'" name="n1_ftw_'+numItems+'" onchange="minus(\'n1_etw_'+numItems+'\',\'n1_ftw_'+numItems+'\',\'n1_sol_'+numItems+'\');" type="text"></td>';
form +='<td><input id="n1_sol_'+numItems+'" name="n1_sol_'+numItems+'" onchange="minus(\'n1_etw_'+numItems+'\',\'n1_ftw_'+numItems+'\',\'n1_sol_'+numItems+'\');" type="text"></td>';
form +='<td rowspan="10"><select  name="result'+numItems+'"  style="height: 30px;" ><option value="pass">pass</option><option value="fail">fail</option></select>&nbsp;&nbsp;</td>';
form +='</tr>';

form +='<tr>';
form +='<td>N2</td>';
form +='<td><input id="n2_etw_'+numItems+'" name="n2_etw_'+numItems+'" onchange="minus(\'n2_etw_'+numItems+'\',\'n2_ftw_'+numItems+'\',\'n2_sol_'+numItems+'\');" type="text"></td>';
form +='<td><input id="n2_ftw_'+numItems+'" name="n2_ftw_'+numItems+'" onchange="minus(\'n2_etw_'+numItems+'\',\'n2_ftw_'+numItems+'\',\'n2_sol_'+numItems+'\');" type="text"></td>';
form +='<td><input id="n2_sol_'+numItems+'" name="n2_sol_'+numItems+'" onchange="minus(\'n2_etw_'+numItems+'\',\'n2_ftw_'+numItems+'\',"n2_sol_'+numItems+'\');" type="text"></td>';
form +='</tr>';

form +='<tr>';
form +='<td>N3</td>';
form +='<td><input id="n3_etw_'+numItems+'" name="n3_etw_'+numItems+'" onchange="minus(\'n3_etw_'+numItems+'\',\'n3_ftw_'+numItems+'\',\'n3_sol_'+numItems+'\');" type="text"></td>';
form +='<td><input id="n3_ftw_'+numItems+'" name="n3_ftw_'+numItems+'" onchange="minus(\'n3_etw_'+numItems+'\',\'n3_ftw_'+numItems+'\',\'n3_sol_'+numItems+'\');" type="text"></td>';
form +='<td><input id="n3_sol_'+numItems+'" name="n3_sol_'+numItems+'" onchange="minus(\'n3_etw_'+numItems+'\',\'n3_ftw_'+numItems+'\',\'n3_sol_'+numItems+'\');" type="text"></td>';
form +='</tr>';

form +='<tr>';
form +='<td>N4</td>';
form +='<td><input id="n4_etw_'+numItems+'" name="n4_etw_'+numItems+'" onchange="minus(\'n4_etw_'+numItems+'\',\'n4_ftw_'+numItems+'\',\'n4_sol_'+numItems+'\');" type="text"></td>';
form +='<td><input id="n4_ftw_'+numItems+'" name="n4_ftw_'+numItems+'" onchange="minus(\'n4_etw_'+numItems+'\',\'n4_ftw_'+numItems+'\',\'n4_sol_'+numItems+'\');" type="text"></td>';
form +='<td><input id="n4_sol_'+numItems+'" name="n4_sol_'+numItems+'" onchange="minus(\'n4_etw_'+numItems+'\',\'n4_ftw_'+numItems+'\',\'n4_sol_'+numItems+'\');" type="text"></td>';
form +='</tr>';

form +='<tr>';
form +='<td>N5</td>';
form +='<td><input id="n5_etw_'+numItems+'" name="n5_etw_'+numItems+'" onchange="minus(\'n5_etw_'+numItems+'\',\'n5_ftw_'+numItems+'\',\'n5_sol_'+numItems+'\');" type="text"></td>';
form +='<td><input id="n5_ftw_'+numItems+'" name="n5_ftw_'+numItems+'" onchange="minus(\'n5_etw_'+numItems+'\',\'n5_ftw_'+numItems+'\',\'n5_sol_'+numItems+'\');" type="text"></td>';
form +='<td><input id="n5_sol_'+numItems+'" name="n5_sol_'+numItems+'" onchange="minus(\'n5_etw_'+numItems+'\',\'n5_ftw_'+numItems+'\',\'n5_sol_'+numItems+'\');" type="text"></td>';
form +='</tr>';

form +='<tr>';
form +='<td>N6</td>';
form +='<td><input id="n6_etw_'+numItems+'" name="n6_etw_'+numItems+'" onchange="minus(\'n6_etw_'+numItems+'\',\'n6_ftw_'+numItems+'\',\'n6_sol_'+numItems+'\');" type="text"></td>';
form +='<td><input id="n6_ftw_'+numItems+'" name="n6_ftw_'+numItems+'" onchange="minus(\'n6_etw_'+numItems+'\',\'n6_ftw_'+numItems+'\',\'n6_sol_'+numItems+'\');" type="text"></td>';
form +='<td><input id="n6_sol_'+numItems+'" name="n6_sol_'+numItems+'" onchange="minus(\'n6_etw_'+numItems+'\',\'n6_ftw_'+numItems+'\',\'n6_sol_'+numItems+'\');" type="text"></td>';
form +='</tr>';

form +='<tr>';
form +='<td>N7</td>';
form +='<td><input id="n7_etw_'+numItems+'" name="n7_etw_'+numItems+'" onchange="minus(\'n7_etw_'+numItems+'\',\'n7_ftw_'+numItems+'\',\'n7_sol_'+numItems+'\');" type="text"></td>';
form +='<td><input id="n7_ftw_'+numItems+'" name="n7_ftw_'+numItems+'" onchange="minus(\'n7_etw_'+numItems+'\',\'n7_ftw_'+numItems+'\',\'n7_sol_'+numItems+'\');" type="text"></td>';
form +='<td><input id="n7_sol_'+numItems+'" name="n7_sol_'+numItems+'" onchange="minus(\'n7_etw_'+numItems+'\',\'n7_ftw_'+numItems+'\',\'n7_sol_'+numItems+'\');" type="text"></td>';
form +='</tr>';

form +='<tr>';
form +='<td>N8</td>';
form +='<td><input id="n8_etw_'+numItems+'" name="n8_etw_'+numItems+'" onchange="minus(\'n8_etw_'+numItems+'\',\'n8_ftw_'+numItems+'\',\'n8_sol_'+numItems+'\');" type="text"></td>';
form +='<td><input id="n8_ftw_'+numItems+'" name="n8_ftw_'+numItems+'" onchange="minus(\'n8_etw_'+numItems+'\',\'n8_ftw_'+numItems+'\',\'n8_sol_'+numItems+'\');" type="text"></td>';
form +='<td><input id="n8_sol_'+numItems+'" name="n8_sol_'+numItems+'" onchange="minus(\'n8_etw_'+numItems+'\',\'n8_ftw_'+numItems+'\',\'n8_sol_'+numItems+'\');" type="text"></td>';
form +='</tr>';


form +='<tr>';
form +='<td>N9</td>';
form +='<td><input id="n9_etw_'+numItems+'" name="n9_etw_'+numItems+'" onchange="minus(\'n9_etw_'+numItems+'\',\'n9_ftw_'+numItems+'\',\'n9_sol_'+numItems+'\');" type="text"></td>';
form +='<td><input id="n9_ftw_'+numItems+'" name="n9_ftw_'+numItems+'" onchange="minus(\'n9_etw_'+numItems+'\',\'n9_ftw_'+numItems+'\',\'n9_sol_'+numItems+'\');" type="text"></td>';
form +='<td><input id="n9_sol_'+numItems+'" name="n9_sol_'+numItems+'" onchange="minus(\'n9_etw_'+numItems+'\',\'n9_ftw_'+numItems+'\',\'n9_sol_'+numItems+'\');" type="text"></td>';
form +='</tr>';


form +='<tr>';
form +='<td>N10</td>';
form +='<td><input id="n10_etw_'+numItems+'" name="n10_etw_'+numItems+'" onchange="minus(\'n10_etw_'+numItems+'\',\'n10_ftw_'+numItems+'\',\'n10_sol_'+numItems+'\');" type="text"></td>';
form +='<td><input id="n10_ftw_'+numItems+'" name="n10_ftw_'+numItems+'" onchange="minus(\'n10_etw_'+numItems+'\',\'n10_ftw_'+numItems+'\',\'n10_sol_'+numItems+'\');" type="text"></td>';
form +='<td><input id="n10_sol_'+numItems+'" name="n10_sol_'+numItems+'" onchange="minus(\'n10_etw_'+numItems+'\',\'n10_ftw_'+numItems+'\',\'n10_sol_'+numItems+'\');" type="text"></td>';
form +='</tr>';

form +='</table>';
form +='</div>';

$('#forms').append(form);
$('#count_forms').val(numItems);
}
</script>
