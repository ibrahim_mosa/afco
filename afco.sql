-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 12, 2016 at 08:24 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 7.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `afco`
--

-- --------------------------------------------------------

--
-- Table structure for table `form`
--

CREATE TABLE `form` (
  `id` int(11) NOT NULL,
  `form_info` longtext NOT NULL,
  `table1` longtext NOT NULL,
  `table2` longtext NOT NULL,
  `table3` longtext NOT NULL,
  `table4` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `form`
--

INSERT INTO `form` (`id`, `form_info`, `table1`, `table2`, `table3`, `table4`) VALUES
(6, '{"doc_no":"1","issue_date":"2016-12-15","effictive_date":"2016-12-14","rev_no":"234","next_rev_date":"2016-12-28","supersede_date":"2","test_start_date":"2016-12-22T01:01","product_name":"mmmmm","batch_no":"33","batch_size":"44","cat_no":"44","expire_date":"2016-12-15","injected_solution":"34","injected_solution2":"1345","id_no1":"qwd","id_no2":"qwd","test_completion_date":"2016-12-01T01:01","tested_by":"aaaaaa"}', '{"result1":"fail","test_date1":"2016-12-15T01:01","n1_etw_1":"42343","n1_ftw_1":"423432","n1_sol_1":"381089","n2_etw_1":"4234","n2_ftw_1":"23","n2_sol_1":"-4211","n3_etw_1":"324","n3_ftw_1":"323242","n3_sol_1":"322918","n4_etw_1":"3242","n4_ftw_1":"234223","n4_sol_1":"230981","n5_etw_1":"2342","n5_ftw_1":"23232","n5_sol_1":"20890","n6_etw_1":"234","n6_ftw_1":"322","n6_sol_1":"88","n7_etw_1":"23","n7_ftw_1":"234","n7_sol_1":"211","n8_etw_1":"323","n8_ftw_1":"43","n8_sol_1":"-280","n9_etw_1":"3242","n9_ftw_1":"2344","n9_sol_1":"-898","n10_etw_1":"324","n10_ftw_1":"2342","n10_sol_1":"2018"}', '{"result2":"fail","test_date2":"2016-12-15T11:11","n1_etw_2":"42","n1_ftw_2":"23423","n1_sol_2":"23381","n2_etw_2":"4234","n2_ftw_2":"2342","n2_sol_2":"-1892","n3_etw_2":"324","n3_ftw_2":"234","n3_sol_2":"-90","n4_etw_2":"234","n4_ftw_2":"324","n4_sol_2":"90","n5_etw_2":"324","n5_ftw_2":"23423","n5_sol_2":"23099","n6_etw_2":"4234","n6_ftw_2":"32","n6_sol_2":"-4202","n7_etw_2":"234","n7_ftw_2":"23423","n7_sol_2":"23189","n8_etw_2":"333","n8_ftw_2":"234","n8_sol_2":"-99","n9_etw_2":"3434","n9_ftw_2":"23443","n9_sol_2":"20009","n10_etw_2":"343423","n10_ftw_2":"3434","n10_sol_2":"-339989"}', '{"result3":"fail","test_date3":"2016-12-16T11:11","n1_etw_3":"213","n1_ftw_3":"234","n1_sol_3":"21","n2_etw_3":"234","n2_ftw_3":"234","n2_sol_3":"","n3_etw_3":"4","n3_ftw_3":"23423","n3_sol_3":"23419","n4_etw_3":"43","n4_ftw_3":"32423","n4_sol_3":"32380","n5_etw_3":"234","n5_ftw_3":"234","n5_sol_3":"","n6_etw_3":"34","n6_ftw_3":"324234","n6_sol_3":"324200","n7_etw_3":"234","n7_ftw_3":"23423","n7_sol_3":"23189","n8_etw_3":"32","n8_ftw_3":"23423","n8_sol_3":"23391","n9_etw_3":"23","n9_ftw_3":"234234","n9_sol_3":"234211","n10_etw_3":"32","n10_ftw_3":"32423","n10_sol_3":"32391"}', '{"result4":"fail","test_date4":"2016-12-13T11:11","n1_etw_4":"2","n1_ftw_4":"23423","n1_sol_4":"23421","n2_etw_4":"2","n2_ftw_4":"32423","n2_sol_4":"32421","n3_etw_4":"432","n3_ftw_4":"494","n3_sol_4":"62","n4_etw_4":"432","n4_ftw_4":"324234","n4_sol_4":"323802","n5_etw_4":"23","n5_ftw_4":"234234","n5_sol_4":"234211","n6_etw_4":"32","n6_ftw_4":"234","n6_sol_4":"202","n7_etw_4":"324234","n7_ftw_4":"324234","n7_sol_4":"324230","n8_etw_4":"32","n8_ftw_4":"234234","n8_sol_4":"234202","n9_etw_4":"32","n9_ftw_4":"23423","n9_sol_4":"23391","n10_etw_4":"32","n10_ftw_4":"234","n10_sol_4":"202"}');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `form`
--
ALTER TABLE `form`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `form`
--
ALTER TABLE `form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
